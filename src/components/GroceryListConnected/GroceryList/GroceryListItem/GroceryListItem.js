import React from 'react'


const GroceryListItem = ({groceryItem , addTodo}) => {
    return (
        <li key = {groceryItem.id} onClick = {() => addTodo(groceryItem)}>
            <span><i className="fa fa-plus" aria-hidden="true"></i></span> 
            <span>{groceryItem.name}</span>
        </li>
    )
}

export default GroceryListItem
