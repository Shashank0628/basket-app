import React  from 'react'
import GroceryListItem from './GroceryListItem/GroceryListItem';
import style from './GroceryStyles.module.css';

const GroceryList = ({groceryItems , addTodo}) => {
    return (
        <div className = {style.groceryContainer}>
            <header>
                <h3><i className="fa fa-leaf" aria-hidden="true"></i> Groceries</h3>
            </header>
            <ul className = {style.groceryList}>
                {
                    groceryItems.map(groceryItem => <GroceryListItem key = {groceryItem.id} groceryItem = {groceryItem} addTodo = {addTodo}/>)
                }
            </ul>
        </div>
    )
}

export default GroceryList
