import {connect} from 'react-redux';
import {addItem} from '../../actions/actions';
import GroceryList from './GroceryList/GroceryList';

const mapStateToProp = state => {
    return {
        groceryItems: state.groceryItems
    }
}

const mapDispatchtoProp = dispatch => {
    return {
        addTodo: (item) => {
            dispatch(addItem(item))
        }
    }
}

const ConnectedComponent = connect(mapStateToProp , mapDispatchtoProp)(GroceryList)

export default ConnectedComponent