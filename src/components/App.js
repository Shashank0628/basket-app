import React  from 'react'
import Header from './Banner/Header/Header';
import Nav from './Banner/Nav/Nav';
import GroceryListConnected from './GroceryListConnected/GroceryListConnected';
import BasketListConnected from './BasketListConnected/BasketListConnected';
import FilterContainer from './Banner/FilterContainer/FilterContainer';
import Footer from './Footer/Footer';

import './App.css';

const App = () => {
    
    return (
        <div className = 'App'>
            <Header />
            <Nav />
            <FilterContainer />
            
            <main>
                <GroceryListConnected />
                <BasketListConnected />
            </main>
            
            <Footer />
        </div>
    )
}

export default App