import React from 'react'
import style from './FilterStyles.module.css';
import FilterLinkConnected from './FilterLinksConnected/FilterLinkConnected';

const FilterContainer = () => {
    return (
        <div className = {style.filterContainer}>
            <FilterLinkConnected filter = "SHOW_ALL">All</FilterLinkConnected>            
            <FilterLinkConnected filter = "SHOW_COMPLETED">Completed</FilterLinkConnected>            
            <FilterLinkConnected filter = "SHOW_ACTIVE">Pending</FilterLinkConnected>            
        </div>
    )
}

export default FilterContainer
