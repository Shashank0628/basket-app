import React  from 'react'
import style from '../FilterStyles.module.css';

const FilterLinks = ({active , setFilter , children}) => {
    
    return (
        <div className = {style.filterItem + " " + (active ? style.activeItem : undefined)} onClick = {() => setFilter()}>
            {children}
        </div>
    )
}

export default FilterLinks
