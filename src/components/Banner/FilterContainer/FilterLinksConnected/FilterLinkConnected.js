import {connect} from 'react-redux';
import {setFilter} from '../../../../actions/actions';
import FilterLinks from '../FilterLinks/FilterLinks';

const mapStateToProp = (state , ownProps) => {
    return {
        active: ownProps.filter === state.displayFilter
    }
}

const mapDispatchToProp = (dispatch , ownProps) => {
    return {
        setFilter: () => {
            dispatch(setFilter(ownProps.filter))
        }
    }
}


const ConnectedComponent = connect(mapStateToProp , mapDispatchToProp)(FilterLinks)

export default ConnectedComponent;