import React from 'react'
import style from '../Banner.module.css';

const Nav = () => {
    return (
        <nav className = {style.nav}>
            <form>
                <input type="text" name='filter' placeholder = 'Enter your filters' defaultValue = "filter"/>
                <button type = 'submit'>Submit</button>
            </form>
        </nav>
    )
}

export default Nav
