import React from 'react'
import style from '../Banner.module.css';

const Header = () => {
    return (
        <header className = {style.appHeader}>
              <i className = 'fa fa-shopping-basket fa-6'></i>
              <h3 className = {style.appTitle}>Hello, Basket</h3>
        </header>
    )
}

export default Header
