import React from 'react'
import style from './FooterStyle.module.css';
import FilterContainer from '../Banner/FilterContainer/FilterContainer';

const Footer = () => {
    return (
        <footer className = {style.footer}>
            <FilterContainer />
        </footer>
    )
}

export default Footer
