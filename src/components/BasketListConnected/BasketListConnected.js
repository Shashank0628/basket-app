import {connect} from 'react-redux';
import {emptyBasket, toggleItem} from '../../actions/actions';
import BasketList from './BasketList/BasketList';

const filterData = (basketItems , filter) => {
    switch(filter){
        case "SHOW_ALL":
            return basketItems;
        
        case "SHOW_COMPLETED":
            let completedBasketItem = basketItems.filter((basketItem) => basketItem.bought)
            return completedBasketItem
        
        case "SHOW_ACTIVE":
            let activeBasketItem = basketItems.filter((basketItem) => !basketItem.bought)
            return activeBasketItem
        
        default: 
            return basketItems;
    }
}

const mapStateToProp = (state) => {
    return {
        basketItems: filterData(state.basketItems , state.displayFilter)
    }
}


const mapDispatchtoProp = (dispatch) => {
    return {
        toggleItem: (id) => {
            dispatch(toggleItem(id))
        } ,
        emptyBasket: () => {
            dispatch(emptyBasket())
        }
    }
}

const ConnectedComponent = connect(mapStateToProp , mapDispatchtoProp)(BasketList)

export default ConnectedComponent