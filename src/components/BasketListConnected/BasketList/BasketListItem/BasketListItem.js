import React from 'react'
import style from '../BasketStyle.module.css';

const BasketListItem = ({basketItem , toggleItem}) => {

    return (
        <li key = {basketItem.id} onClick = {() => toggleItem(basketItem.id)} className = {basketItem.bought ? style.toggled : undefined}>
           <span><i className="fa fa-minus" aria-hidden="true"></i></span>  
           <span>{basketItem.count}</span> 
           <span>{basketItem.name} </span>
        </li>
    )
}

export default BasketListItem
