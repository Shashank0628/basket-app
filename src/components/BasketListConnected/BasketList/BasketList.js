import React from 'react'
import style from './BasketStyle.module.css';
import BasketListItem from './BasketListItem/BasketListItem';

const BasketList = ({basketItems , toggleItem , emptyBasket}) => {
    let isClear = !basketItems.length

    return (
        <div className = {style.basketContainer} >
            <header>
                <h3>
                    <i className="fa fa-shopping-basket" aria-hidden="true"></i> Basket
                </h3>
                { !isClear && <button onClick = {() => emptyBasket()}><i className = 'fa fa-trash'></i></button>
                }
            </header>
            <section>
                {
                    isClear ? (
                        <ul>
                            <li style = {{background: "transparent"}}>Your Basket is Empty</li>
                        </ul>
                    ) : (
                        <ul>
                            {basketItems.map(basketItem => <BasketListItem key = {basketItem.id} basketItem = {basketItem} toggleItem = {toggleItem}/>)}
                        </ul>
                    )
                }
            </section>
        </div>
    )
}

export default BasketList
