export const addItem = (item) => {
    return {
        type: "ADD_ITEM",
        payload: item
    }
}

export const toggleItem = (id) => {
    return {
        type: "TOGGLE_ITEM",
        payload: id
    }
}

export const emptyBasket = () => {
    return {
        type: "EMPTY_BASKET",
    }
}

export const setFilter = (filter) => {
    return {
        type: "SET_FILTER",
        payload: filter
    }
}

