import { combineReducers } from "redux";

const initialState = {
    displayFilter: "SHOW_ALL",
    todos: [],
    categories: [
        {id:1, name:'Vegetable'},
		{id:2, name:'Fruits'},
		{id:3, name:'Cereals'},
		{id:4, name:'Meat'},
		{id:5, name:'Dairy'}
    ] ,
    availableItems: [
		{id:1, name:'Strawberry', categoryId:2},
		{id:2, name:'Blueberry', categoryId:2},
		{id:3, name:'Orange', categoryId:2},
		{id:4, name:'Banana', categoryId:2},
		{id:5, name:'Apple', categoryId:2},

		{id:6, name:'Carrot', categoryId:1},
		{id:7, name:'Celery', categoryId:1},
		{id:8, name:'Mushroom', categoryId:1},
		{id:9, name:'Green Pepper', categoryId:1},

		{id:10, name:'Eggs', categoryId:5},
		{id:11, name:'Cheese', categoryId:5},
		{id:12, name:'Butter', categoryId:5},

		{id:13, name:'Chicken', categoryId:4},
		{id:14, name:'Beef', categoryId:4},
		{id:15, name:'Pork', categoryId:4},
		{id:16, name:'Fish', categoryId:4},

		{id:17, name:'Rice', categoryId:3},
		{id:18, name:'Pasta', categoryId:5},
		{id:19, name:'Bread', categoryId:5}
	] , 
    list: [
        {id:1, name:'Strawberry', count:1, bought:false},
		{id:6, name:'Carrot', count:1, bought:false},
		{id:10, name:'Eggs', count:1, bought:false},
		{id:13, name:'Chicken', count:1, bought:false},
		{id:17, name:'Rice', count:1, bought:false}
    ] 
}

const groceryItems = (state = initialState.availableItems , action) => {
    switch(action.type){
        default:
            return state
    }
}

const basketItems = (state = initialState.list , action) => {
    switch(action.type){
        case "ADD_ITEM":
            let itemExisted = false
            let newList = state.map(basketItem => {
                                if(basketItem.id === action.payload.id){
                                    itemExisted = true
                                    return {...basketItem , count: basketItem.count + 1}
                                }
                                else{
                                    return basketItem
                                }
                            })
            
            if(itemExisted){
                return newList
            }
            else{
                return [...state , {...action.payload , count: 1 , bought: false}]
            }

        case "TOGGLE_ITEM":
            let ntList = state.map( basketItem => (
                basketItem.id === action.payload ?
                {...basketItem , bought: !basketItem.bought} :
                basketItem ) )
            return ntList

        case "EMPTY_BASKET":
            return []

        default: 
            return state
    }
}

const displayFilter = (state = "SHOW_ALL" , action) => {
    switch(action.type){
        case "SET_FILTER":
            return action.payload

        default: 
            return state
    }
}


export const combinedReducer = combineReducers({
                                groceryItems,
                                basketItems,
                                displayFilter
                             })