import React from 'react';
import {createStore} from 'redux';
import {Provider} from 'react-redux';
import {combinedReducer} from './reducers/reducers';
import ReactDOM from 'react-dom';
import App from './components/App.js';
import './index.css';


const store = createStore(combinedReducer , window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())
  

ReactDOM.render(
    <Provider store = {store}>
        <App />
    </Provider> , 
document.querySelector("#root"))

